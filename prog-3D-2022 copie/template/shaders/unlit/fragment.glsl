#version 330 core

in vec3 positionWorld;
in vec3 normalWorld;
in vec2 uv0World;
in mat3 TNB;

out vec4 FragColor;

uniform vec4 color;

uniform vec3 lightColor;
uniform vec3 lightPosition;

struct Material {
    vec3 ambiant;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

uniform Material material;

uniform sampler2D normal;


void main() {
    vec3 normalMap = texture(normal, uv0World).rgb;
    normalMap = normalMap * 2.0 - 1.0;
    normalMap = normalize(TNB * normalMap);

    vec3 ambiant = material.ambiant * lightColor;

    vec3 lightDirection = normalize(lightPosition - positionWorld);

    float diff = max(dot(normalMap, lightDirection), 0.0);
    vec3 diffuse = (diff * material.diffuse) * lightColor;

    vec3 viewDir = normalize(-positionWorld);
    vec3 reflectDir = reflect(-lightDirection, normalMap);

    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = (material.specular * spec) * lightColor;

    vec4 result = vec4((ambiant + diffuse + specular), 1.0) * color;
    FragColor = result;

}
