#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 tangent;
layout(location = 3) in vec2 uv0;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 positionWorld;
out vec3 normalWorld;
out vec2 uv0World;
out mat3 TNB;


void main() {
  positionWorld = vec3(model * vec4(position, 1.0));
  gl_Position = projection * view * vec4(positionWorld, 1.0);

  mat3 normalMatrix = mat3(transpose(inverse(model)));
  normalWorld = normalMatrix * normal;
  uv0World = uv0;

  vec3 T = normalize(vec3(model * vec4(tangent, 0.0)));
  vec3 N = normalize(vec3(model * vec4(normal, 0.0)));
  vec3 B = cross(N, T);
  TNB = mat3(T, B, N);

}
